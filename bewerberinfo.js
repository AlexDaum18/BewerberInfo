function unterlagenTabelleErstellen(bewerberId) {

    $.ajax({
        type: "GET",
        data: {bewerberId: bewerberId},
        url: 'ajax/ladeUnterlagenVonBewerber.php',
        success: function(data) {
            $("#unterlagenBox").html(data);
        }
    });
}


function vorschauBoxErstellen(unterlagenId) {

    $.ajax({
       type: "GET",
       data: {unterlagenId: unterlagenId},
       url: 'ajax/ladeVorschauVonUnterlagen.php',
       success: function(data) {
           $("#vorschauBox").html(data);
       }
    });
}