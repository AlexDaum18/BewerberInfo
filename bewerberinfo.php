<!DOCTYPE html>
<html>
<head>
	<title>Bewerber Info</title>
	<link rel="stylesheet" type="text/css" href="style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="bewerberinfo.js"></script>

</head>
<body>



    <?php
    require("inc/basis.php");
    ?>




	
	<!-- Verbindung mit der Datenbank -->
	
	<?php

	//bewerbertabelle

    $query = "SELECT * FROM `bewerber`";

	$alleBewerber = mysqli_query($dbconnect, $query);

    if(!$alleBewerber){
        echo $query;
    } else {
        $alleBewerber = $alleBewerber->fetch_all(MYSQLI_ASSOC);
    }

	?>
	
	<!-- Bewerber Box -->
	
	<div class="header">
		<h2>Bewerber</h2>
	</div>
	
	
	<table width="400" border="0">
		<tr>
			<th>Vorname</th>
			<th>Nachname</th>
			<th>Details</th>
		</tr>
	</table>

	<?php
	echo '<div style="width:420px; height:100px; overflow:auto;">';
	echo '<table width="400" border="0">';

    foreach ($alleBewerber as $unterlagen) {

		echo '<tr>';
		echo '<td>';	
		echo $unterlagen['Vorname'];
		echo '</td>';
		echo '<td>';
		echo $unterlagen['Name'];
		echo '</td>';
		
		echo '<td>';
		$bewerberIdAktuell = $unterlagen['Id'];
		$btn1 = 'btn1';
		echo "<button id=$btn1 name=$btn1 onclick='unterlagenTabelleErstellen($bewerberIdAktuell)'>Detail</button>";

		echo '</td>';
		echo '</tr>';

	}

	echo '</table>';
	echo '</div>';
	?>

	<input type="button" value="Neu" onclick="window.location.href='neuerBewerber.php'" />
	
	
	
	
	<!-- Unterlagen Box -->
	
	<div class="header">
		<h2>Unterlagen</h2>
	</div>

	<table width="400" border="0">
		<tr>
			<th>Typ</th>
			<th>Datum</th>
			<th>Details</th>
		</tr>
	</table>
	
	<div id="unterlagenBox" style="width:420px; height:100px; overflow:auto;">

    </div>


	<input type="button" value="Neue Unterlage" onclick="window.location.href='neueUnterlage.php'" />
	



	<!-- Vorschau Box -->
	
	<div class="header">
		<h2>Vorschau</h2>
	</div>

    <div id="vorschauBox">

    </div>



	
</body>
</html>


